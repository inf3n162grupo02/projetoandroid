package br.com.appclinica.appclinica.model;

/**
 * Created by Aluno on 09/04/2018.
 */

public class Convenio {

    public int id;
    public String nome;

    public Convenio() {
    }

    public Convenio(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return nome;
    }
}
