package br.com.appclinica.appclinica.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import br.com.appclinica.appclinica.R;

public class AreaTrabalhoActivity extends AppCompatActivity {

    private Button btnCadPaciente;
    private Button btnListarPacientes;
    private Button btnAgenda;
    private Button btnListarAgenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_trabalho);

        btnCadPaciente = (Button) findViewById(R.id.btnCadPaciente);
        btnListarPacientes = (Button) findViewById(R.id.btnListarPacientes);
        btnAgenda = (Button) findViewById(R.id.btnAgenda);
        btnListarAgenda = (Button) findViewById(R.id.btnListarAgenda);

        btnCadPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAbrirCadPaciente = new Intent(AreaTrabalhoActivity.this, CadPacientesActivity.class);
                intentAbrirCadPaciente.putExtra("acao", "inserir");
                startActivity(intentAbrirCadPaciente);
            }
        });

        btnListarPacientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentListarPacientes = new Intent(AreaTrabalhoActivity.this, ListarPacientesActivity.class);
                startActivity(intentListarPacientes);
            }
        });

        btnAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAbrirAgenda = new Intent(AreaTrabalhoActivity.this, AgendaActivity.class);
                startActivity(intentAbrirAgenda);
            }
        });

        btnListarAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentListarAgenda = new Intent(AreaTrabalhoActivity.this, ListarAgendaActivity.class);
                startActivity(intentListarAgenda);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_anotacoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.sair) {
            FirebaseAuth.getInstance().signOut();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
