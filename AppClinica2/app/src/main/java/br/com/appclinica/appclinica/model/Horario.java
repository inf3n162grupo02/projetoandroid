package br.com.appclinica.appclinica.model;

/**
 * Created by Aluno on 09/04/2018.
 */

public class Horario {

    private int id;
    public String horario;

    public Horario() {
    }

    public Horario(int id, String nome) {
        this.id = id;
        this.horario = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return horario;
    }

    public void setNome(String nome) {
        this.horario = nome;
    }

    @Override
    public String toString() {
        return horario;
    }
}

