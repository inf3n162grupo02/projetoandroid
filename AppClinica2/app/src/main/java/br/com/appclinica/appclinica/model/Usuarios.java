package br.com.appclinica.appclinica.model;

/**
 * Created by Aluno on 02/04/2018.
 */

public class Usuarios {

    String email, senha;

    public Usuarios() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
