package br.com.appclinica.appclinica.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

import br.com.appclinica.appclinica.R;
import br.com.appclinica.appclinica.model.Agendamento;
import br.com.appclinica.appclinica.model.Paciente;

public class ListarAgendaActivity extends AppCompatActivity {

    private ListView lvAgendamentos;
    private List<Agendamento> listaAgendamento;
    private ChildEventListener childEventListener;
    private Query queryRef;
    private FirebaseUser paciente = FirebaseAuth.getInstance().getCurrentUser();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_agenda);

        lvAgendamentos = (ListView) findViewById(R.id.lvAgendamentos);
        listaAgendamento = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (paciente == null) {
            finish();
        }
        buscarAgendamentos();
        queryRef.addChildEventListener(childEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        queryRef.removeEventListener(childEventListener);
    }

    private void buscarAgendamentos() {
        listaAgendamento.clear();
        queryRef = ref.child("agendamentos").orderByChild("nome");

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Agendamento agendamento = new Agendamento();
                agendamento.nome = dataSnapshot.child("nome").getValue(String.class);
                agendamento.horario = dataSnapshot.child("horario").getValue(String.class);
                listaAgendamento.add(agendamento);
                ArrayAdapter adapter = new ArrayAdapter(ListarAgendaActivity.this, android.R.layout.simple_list_item_1, listaAgendamento);
                lvAgendamentos.setAdapter(adapter);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

    }
}
