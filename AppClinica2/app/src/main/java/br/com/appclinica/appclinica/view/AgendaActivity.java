package br.com.appclinica.appclinica.view;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.appclinica.appclinica.R;
import br.com.appclinica.appclinica.model.Exame;
import br.com.appclinica.appclinica.model.Horario;
import br.com.appclinica.appclinica.model.Medico;
import br.com.appclinica.appclinica.model.Paciente;

public class AgendaActivity extends AppCompatActivity {

    private AutoCompleteTextView acPacientes;
    private RadioButton rbExame, rbConsulta, rbReconsulta;
    private Spinner spMedico, spExame, spHorario;
    private EditText etObservacao;
    private Button btnSalvarAgendamento;
    private RadioGroup rgTipoAgendamento;
    private DatePicker dpDataAgendada;

    private ChildEventListener pacienteChildEventListener, exameChildEventListener, medicoChildEventListener, horarioChildEventListener;
    private Query pacienteQueryRef, exameQueryRef, medicoQueryRef, horarioQueryRef;
    private FirebaseUser pac = FirebaseAuth.getInstance().getCurrentUser();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference();

    private List<Medico> listaMedicos;
    private List<Exame> listaExames;
    private List<Horario> listaHorarios;

    private List<Paciente> listAcPacientes;
    private Paciente paciente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        acPacientes = (AutoCompleteTextView) findViewById(R.id.acPacientes);

        rgTipoAgendamento = (RadioGroup) findViewById(R.id.rgTipoAgendamento);

        dpDataAgendada = (DatePicker) findViewById(R.id.dpDataAgendada);

        rbExame = (RadioButton) findViewById(R.id.rbExame);
        rbConsulta = (RadioButton) findViewById(R.id.rbConsulta);
        rbReconsulta = (RadioButton) findViewById(R.id.rbReconsulta);


        spMedico = (Spinner) findViewById(R.id.spMedico);
        spExame = (Spinner) findViewById(R.id.spExame);
        spHorario = (Spinner) findViewById(R.id.spHorario);

        listaMedicos = new ArrayList<>();
        listaExames = new ArrayList<>();
        listaHorarios = new ArrayList<>();
        listAcPacientes = new ArrayList<>();

        etObservacao = (EditText) findViewById(R.id.etObservacao);

        btnSalvarAgendamento = (Button) findViewById(R.id.btnSalvarAgendamento);

        btnSalvarAgendamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarAgendamento();
            }
        });

        acPacientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                paciente = (Paciente) parent.getItemAtPosition(position);
            }
        });

        rgTipoAgendamento.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (rbExame.isChecked()) {
                    spExame.setEnabled(true);
                }
                if (rbConsulta.isChecked() || rbReconsulta.isChecked()) {
                    spExame.setEnabled(false);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (pac == null) {
            finish();
        }
        dataAtual();
        spExame.setEnabled(false);
        buscarMedicos();
        buscarExames();
        buscarHorarios();
        buscarPacientes();
        pacienteQueryRef.addChildEventListener(pacienteChildEventListener);
        horarioQueryRef.addChildEventListener(horarioChildEventListener);
        medicoQueryRef.addChildEventListener(medicoChildEventListener);
        exameQueryRef.addChildEventListener(exameChildEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        pacienteQueryRef.removeEventListener(pacienteChildEventListener);
        horarioQueryRef.removeEventListener(horarioChildEventListener);
        medicoQueryRef.removeEventListener(medicoChildEventListener);
        exameQueryRef.removeEventListener(exameChildEventListener);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        limpar();
    }

    private void buscarMedicos() {
        listaMedicos.clear();
        Medico fake = new Medico();
        fake.setNome("Selecione um médico...");
        listaMedicos.add(fake);

        medicoQueryRef = ref.child("medicos").orderByChild("nome");

        medicoChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Medico medico = new Medico();
                medico.nome = dataSnapshot.child("nome").getValue(String.class);
                listaMedicos.add(medico);
                ArrayAdapter adapter = new ArrayAdapter(AgendaActivity.this, android.R.layout.simple_spinner_dropdown_item, listaMedicos);
                spMedico.setAdapter(adapter);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void buscarExames() {
        listaExames.clear();
        Exame fake = new Exame();
        fake.setNome("Selecione um exame...");
        listaExames.add(fake);

        exameQueryRef = ref.child("exames").orderByChild("nome");

        exameChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Exame exame = new Exame();
                exame.nome = dataSnapshot.child("nome").getValue(String.class);
                listaExames.add(exame);
                ArrayAdapter adapter = new ArrayAdapter(AgendaActivity.this, android.R.layout.simple_spinner_dropdown_item, listaExames);
                spExame.setAdapter(adapter);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void buscarHorarios() {
        listaHorarios.clear();
        Horario fake = new Horario();
        fake.setNome("Selecione um horário...");
        listaHorarios.add(fake);
        horarioQueryRef = ref.child("horarios").orderByChild("horario");

        horarioChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Horario horario = new Horario();
                horario.horario = dataSnapshot.child("horario").getValue(String.class);
                listaHorarios.add(horario);
                ArrayAdapter adapter = new ArrayAdapter(AgendaActivity.this, android.R.layout.simple_spinner_dropdown_item, listaHorarios);
                spHorario.setAdapter(adapter);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void buscarPacientes() {
        listAcPacientes.clear();
        pacienteQueryRef = ref.child("pacientes").orderByChild("nome");

        pacienteChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Paciente paciente = new Paciente();
                paciente.nome = dataSnapshot.child("nome").getValue(String.class);
                paciente.cpf = dataSnapshot.child("cpf").getValue(String.class);
                listAcPacientes.add(paciente);
                ArrayAdapter adapter = new ArrayAdapter(AgendaActivity.this, android.R.layout.simple_spinner_dropdown_item, listAcPacientes);
                acPacientes.setAdapter(adapter);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void salvarAgendamento() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(this);
        alerta.setTitle("Agendamento: ");
        alerta.setIcon(android.R.drawable.ic_menu_edit);

        alerta.setNeutralButton("Cancelar", null);
        alerta.setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Map<String, String> pac = new HashMap<>();

                if (pac != null) {
                    pac.put("nome", paciente.getNome());
                    pac.put("cpf", paciente.getCpf());
                    pac.put("medico", spMedico.getSelectedItem().toString());
                    if (rbExame.isChecked()) {
                        pac.put("exame", spExame.getSelectedItem().toString());
                    } else {
                        pac.put("exame", null);
                    }
                    pac.put("horario", spHorario.getSelectedItem().toString());
                    pac.put("observacao", etObservacao.getText().toString());

                    String erro = "";
                    if (paciente == null) {
                        erro = "Nome";
                    } else {
                        if (spMedico.getSelectedItemPosition() == 0) {
                            erro = "Médico";
                        } else {
                            if (rbExame.isChecked() && spExame.getSelectedItemPosition() == 0) {
                                erro = "Exame";
                            } else {
                                if (spHorario.getSelectedItemPosition() == 0) {
                                    erro = "Horario";
                                }
                            }
                        }
                    }

                    int dia = dpDataAgendada.getDayOfMonth();
                    int mes = dpDataAgendada.getMonth();
                    int ano = dpDataAgendada.getYear();

                    pac.put("dia", String.valueOf(dia));
                    pac.put("mes", String.valueOf(mes));
                    pac.put("ano", String.valueOf(ano));

                    if (!erro.isEmpty()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(AgendaActivity.this);
                        alert.setTitle("Atenção!!!");
                        alert.setIcon(android.R.drawable.ic_dialog_alert);
                        alert.setMessage("Você deve selecionar o campo " + erro);
                        alert.setNeutralButton("OK", null);
                        alert.show();
                    } else {
                        ref.child("agendamentos").push().setValue(pac);
                        Toast.makeText(AgendaActivity.this, "Agendado com sucesso", Toast.LENGTH_LONG).show();
                        limpar();
                    }
                }

            }


        });
        alerta.show();


    }

    private void dataAtual(){
        Calendar dataAtual = Calendar.getInstance();
        dpDataAgendada.updateDate( dataAtual.get(Calendar.YEAR), dataAtual.get(Calendar.MONTH), dataAtual.get(Calendar.DAY_OF_MONTH));
    }

    public void limpar() {
        acPacientes.setText("");
        rbExame.setChecked(false);
        rbConsulta.setChecked(false);
        rbReconsulta.setChecked(false);
        spMedico.setSelection(0);
        spExame.setSelection(0);
        spHorario.setSelection(0);
        etObservacao.setText("");
        dataAtual();
    }

}
