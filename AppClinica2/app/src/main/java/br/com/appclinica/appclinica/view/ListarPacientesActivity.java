package br.com.appclinica.appclinica.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.appclinica.appclinica.R;
import br.com.appclinica.appclinica.model.Paciente;
import br.com.appclinica.appclinica.network.ConfiguracaoFirebase;

public class ListarPacientesActivity extends AppCompatActivity {

    private ListView lvPacientes;
    private List<Paciente> listaPacientes;

    private Paciente p;
    private Paciente pacienteExcluir;

    private ChildEventListener childEventListener;
    private Query queryRef;
    private FirebaseUser paciente = FirebaseAuth.getInstance().getCurrentUser();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_pacientes);

        lvPacientes = (ListView) findViewById(R.id.lvPacientes);
        listaPacientes = new ArrayList<>();

        lvPacientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                p = (Paciente) lvPacientes.getItemAtPosition(position);
                Intent intentAbrirCadPaciente = new Intent(ListarPacientesActivity.this, CadPacientesActivity.class);
                intentAbrirCadPaciente.putExtra("acao", "editar");
                intentAbrirCadPaciente.putExtra("idPaciente", p.getId());
                startActivity(intentAbrirCadPaciente);

            }
        });

        lvPacientes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                pacienteExcluir = (Paciente) adapterView.getItemAtPosition(i);
                AlertDialog.Builder alerta = new AlertDialog.Builder(ListarPacientesActivity.this);
                alerta.setTitle("Excluir Paciente: ");
                alerta.setIcon(android.R.drawable.ic_menu_edit);

                alerta.setNeutralButton("Cancelar", null);
                alerta.setPositiveButton("Excluir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ref.child("pacientes").child(pacienteExcluir.getId()).removeValue();
                        Toast.makeText(ListarPacientesActivity.this, "Dados excluídos com sucesso", Toast.LENGTH_LONG).show();
                    }
                });

                alerta.show();
                return true;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (paciente == null) {
            finish();
        }
        buscarPacientes();
        queryRef.addChildEventListener(childEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        queryRef.removeEventListener(childEventListener);
    }

    @Override
    protected void onRestart() {
        buscarPacientes();
        super.onRestart();
    }

    private void buscarPacientes() {
        listaPacientes.clear();
        queryRef = ref.child("pacientes").orderByChild("nome");

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Paciente paciente = new Paciente();

                paciente.id = dataSnapshot.getKey();
                paciente.nome = dataSnapshot.child("nome").getValue(String.class);
                listaPacientes.add(paciente);
                ArrayAdapter adapter = new ArrayAdapter(ListarPacientesActivity.this, android.R.layout.simple_list_item_1, listaPacientes);
                lvPacientes.setAdapter(adapter);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

    }


}
