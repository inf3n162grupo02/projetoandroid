package br.com.appclinica.appclinica.model;

/**
 * Created by aluno on 29/03/2018.
 */

public class Agendamento {

    private int id;
    public String nome, horario;

    public Agendamento() {
    }

    public Agendamento(int id, String horario, String nome) {
        this.id = id;
        this.horario = horario;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return ("Paciente: " + nome + "\n" + "Horario: " + horario);
    }
}
