package br.com.appclinica.appclinica.view;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.appclinica.appclinica.R;
import br.com.appclinica.appclinica.adapters.Mascaras;
import br.com.appclinica.appclinica.model.Cidade;
import br.com.appclinica.appclinica.model.Convenio;
import br.com.appclinica.appclinica.model.Paciente;

public class CadPacientesActivity extends AppCompatActivity {

    private EditText etNome, etDataNascimento, etTelefone, etCpf, etEmail, etLogradouro, etComplemento, etNumero, etCep, etObservacao;
    private RadioButton rbMasculino, rbFeminino;
    private Spinner spConvenio, spCidade;
    private ImageView ivFoto;
    private Bitmap bitmap;
    private Button btnSalvar, btnFoto;


    private ChildEventListener cidadeChildEventListener, convenioChildEventListener, pacienteChildEventListener;
    private Query cidadeQueryRef, convenioQueryRef, pacienteQueryRef;
    private FirebaseUser paciente = FirebaseAuth.getInstance().getCurrentUser();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference();

    private List<Cidade> listaCidades;
    private List<Convenio> listaConvenios;

    private String acao, idPaciente;
    private Paciente pacienteAtual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_pacientes);

        acao = getIntent().getExtras().getString("acao");
        idPaciente = getIntent().getExtras().getString("idPaciente");

        etNome = (EditText) findViewById(R.id.etNome);

        etDataNascimento = (EditText) findViewById(R.id.etDataNascimento);
        etDataNascimento.addTextChangedListener(Mascaras.mask(etDataNascimento, Mascaras.FORMAT_DATE));

        etTelefone = (EditText) findViewById(R.id.etTelefone);
        etTelefone.addTextChangedListener(Mascaras.mask(etTelefone, Mascaras.FORMAT_FONE));

        etCpf = (EditText) findViewById(R.id.etCpf);
        etCpf.addTextChangedListener(Mascaras.mask(etCpf, Mascaras.FORMAT_CPF));

        etEmail = (EditText) findViewById(R.id.etEmail);
        etLogradouro = (EditText) findViewById(R.id.etLogradouro);
        etComplemento = (EditText) findViewById(R.id.etComplemento);
        etNumero = (EditText) findViewById(R.id.etNumero);

        etCep = (EditText) findViewById(R.id.etCep);
        etCep.addTextChangedListener(Mascaras.mask(etCep, Mascaras.FORMAT_CEP));

        etObservacao = (EditText) findViewById(R.id.etObservacao);

        rbMasculino = (RadioButton) findViewById(R.id.rbMasculino);
        rbFeminino = (RadioButton) findViewById(R.id.rbFeminino);

        spConvenio = (Spinner) findViewById(R.id.spConvenio);
        spCidade = (Spinner) findViewById(R.id.spCidade);

        listaConvenios = new ArrayList<>();
        listaCidades = new ArrayList<>();

        btnFoto = (Button) findViewById(R.id.btnFoto);
        ivFoto = (ImageView) findViewById(R.id.ivFoto);
        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirCamera();
            }
        });

        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (acao.equals("inserir")) {
                    salvarPaciente();
                }
                if (acao.equals("editar")) {
                    atualizarPaciente();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (paciente == null) {
            finish();
        }
        permissoes();
        buscarCidades();
        buscarConvenio();

        if (acao.equals("editar")) {
            carregarPaciente();
            pacienteQueryRef.addChildEventListener(pacienteChildEventListener);

            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setMessage("Paciente carregado!!");
            alerta.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    for (int i = 0; i < listaCidades.size(); i++) {
                        if (pacienteAtual.cidade.nome.equals(listaCidades.get(i).nome)) {
                            spCidade.setSelection(i);
                        }
                    }
                    for (int i = 0; i < listaConvenios.size(); i++) {
                        if (pacienteAtual.convenio.nome.equals(listaConvenios.get(i).nome)) {
                            spConvenio.setSelection(i);
                        }
                    }
                }
            });
            alerta.show();
        }
        cidadeQueryRef.addChildEventListener(cidadeChildEventListener);
        convenioQueryRef.addChildEventListener(convenioChildEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (acao.equals("inserir")) {
            cidadeQueryRef.removeEventListener(cidadeChildEventListener);
            convenioQueryRef.removeEventListener(convenioChildEventListener);
        }

        if (acao.equals("editar")) {
            carregarPaciente();
            pacienteQueryRef.removeEventListener(pacienteChildEventListener);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        limpar();
    }

    private void permissoes() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    public void abrirCamera() {
        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camera, 0);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 0 && resultCode == RESULT_OK) {
                try {
                    if (bitmap != null) {
                        bitmap.recycle();
                    }
                    Uri uri = data.getData();
                    stream = getContentResolver().openInputStream(uri);
                    bitmap = BitmapFactory.decodeStream(stream);
                    ivFoto.setImageBitmap(resizeImage(this, bitmap, 700, 600));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    if (stream != null)
                        try {
                            stream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    }


    private static Bitmap resizeImage(Context context, Bitmap bmpOriginal, float newWidth, float newHeight) {
        Bitmap novoBmp = null;

        int w = bmpOriginal.getWidth();
        int h = bmpOriginal.getHeight();

        float densityFactor = context.getResources().getDisplayMetrics().density;
        float novoW = newWidth * densityFactor;
        float novoH = newHeight * densityFactor;

        //Calcula escala em percentual do tamanho original para o novo tamanho
        float scalaW = novoW / w;
        float scalaH = novoH / h;

        //Criando uma matrix para manipulação de imagem Bitmap
        Matrix matrix = new Matrix();

        //Definindo a prorporção da escala para o matrix
        matrix.postScale(scalaW, scalaH);

        //Criando novo Bitmap com o novo tamanho
        novoBmp = Bitmap.createBitmap(bmpOriginal, 0, 0, w, h, matrix, true);

        return novoBmp;

    }

    private void buscarCidades() {
        listaCidades.clear();
        Cidade fake = new Cidade();
        fake.setNome("Selecione uma cidade...");
        listaCidades.add(fake);

        cidadeQueryRef = ref.child("cidades").orderByChild("nome");

        cidadeChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Cidade cidade = new Cidade();
                cidade.nome = dataSnapshot.child("nome").getValue(String.class);
                cidade.id = dataSnapshot.child("id").getValue(Integer.class);
                listaCidades.add(cidade);
                ArrayAdapter adapter = new ArrayAdapter(CadPacientesActivity.this, android.R.layout.simple_spinner_dropdown_item, listaCidades);
                spCidade.setAdapter(adapter);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void buscarConvenio() {
        listaConvenios.clear();
        Convenio fake = new Convenio();
        fake.setNome("Selecione um convênio...");
        listaConvenios.add(fake);

        convenioQueryRef = ref.child("convenios").orderByChild("nome");

        convenioChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Convenio convenio = new Convenio();
                convenio.nome = dataSnapshot.child("nome").getValue(String.class);
                convenio.setId(dataSnapshot.child("id").getValue(Integer.class));
                listaConvenios.add(convenio);
                ArrayAdapter adapter = new ArrayAdapter(CadPacientesActivity.this, android.R.layout.simple_spinner_dropdown_item, listaConvenios);
                spConvenio.setAdapter(adapter);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void salvarPaciente() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(this);
        alerta.setTitle("Dados Paciente: ");
        alerta.setIcon(android.R.drawable.ic_menu_edit);

        alerta.setNeutralButton("Cancelar", null);
        alerta.setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//
                Paciente paciente = new Paciente();
                paciente.setNome(etNome.getText().toString());
                paciente.setTelefone(etTelefone.getText().toString());
                paciente.setCep(etCep.getText().toString());
                paciente.setComplemento(etComplemento.getText().toString());
                paciente.setCpf(etCpf.getText().toString());
                paciente.setDataNascimento(etDataNascimento.getText().toString());
                paciente.setEmail(etEmail.getText().toString());
                paciente.setLogradouro(etLogradouro.getText().toString());
                paciente.setNumero(etNumero.getText().toString());
                paciente.setObservacao(etObservacao.getText().toString());
                paciente.setCidade((Cidade) spCidade.getSelectedItem());
                paciente.setConvenio((Convenio) spConvenio.getSelectedItem());

                String erro = "";
                if (paciente == null) {
                    erro = "Nome";
                } else {
                    if (spCidade.getSelectedItemPosition() == 0) {
                        erro = "Cidade";
                    } else {
                        if (spConvenio.getSelectedItemPosition() == 0) {
                            erro = "Convenio";
                        }
                    }
                }

                String sexo = "";
                if (rbFeminino.isChecked())
                    sexo = Paciente.FEMININO;
                if (rbMasculino.isChecked())
                    sexo = Paciente.MASCULINO;
                paciente.setSexo(sexo);

                if (!erro.isEmpty()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CadPacientesActivity.this);
                    alert.setTitle("Atenção!!!");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("Você deve selecionar o campo " + erro);
                    alert.setNeutralButton("OK", null);
                    alert.show();
                } else {
                    ref.child("pacientes").push().setValue(paciente);
                    Toast.makeText(CadPacientesActivity.this, "Dados salvos com sucesso", Toast.LENGTH_LONG).show();

                    limpar();
                }
            }
        });
        alerta.show();
    }


    public void carregarPaciente() {
        pacienteQueryRef = ref.child("pacientes");
        pacienteChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.getKey().equals(idPaciente)) {
                    Paciente p = new Paciente();
                    p.setId(idPaciente);
                    p.nome = dataSnapshot.child("nome").getValue(String.class);
                    p.numero = dataSnapshot.child("numero").getValue(String.class);
                    p.cpf = dataSnapshot.child("cpf").getValue(String.class);
                    p.cep = dataSnapshot.child("cep").getValue(String.class);
                    p.observacao = dataSnapshot.child("observacao").getValue(String.class);
                    p.logradouro = dataSnapshot.child("logradouro").getValue(String.class);
                    p.email = dataSnapshot.child("email").getValue(String.class);
                    p.sexo = dataSnapshot.child("sexo").getValue(String.class);
                    p.dataNascimento = dataSnapshot.child("dataNascimento").getValue(String.class);
                    p.telefone = dataSnapshot.child("telefone").getValue(String.class);
                    p.complemento = dataSnapshot.child("complemento").getValue(String.class);
                    Cidade c = new Cidade();
                    c.setNome(dataSnapshot.child("cidade").child("nome").getValue(String.class));
                    c.setId(dataSnapshot.child("cidade").child("id").getValue(Integer.class));
                    p.cidade = c;
                    Convenio cv = new Convenio();
                    cv.setNome(dataSnapshot.child("convenio").child("nome").getValue(String.class));
                    cv.setId(dataSnapshot.child("convenio").child("id").getValue(Integer.class));
                    p.convenio = cv;

                    etNome.setText(p.getNome());
                    etCep.setText(p.getCep());
                    etComplemento.setText(p.getComplemento());
                    etCpf.setText(p.getCpf());
                    etDataNascimento.setText(p.getDataNascimento());
                    etEmail.setText(p.getEmail());
                    etLogradouro.setText(p.getLogradouro());
                    etNumero.setText(p.getNumero());
                    etObservacao.setText(p.getObservacao());
                    etTelefone.setText(p.getTelefone());

                    pacienteAtual = p;

                    if (p.getSexo().equals(Paciente.FEMININO)) {
                        rbFeminino.setChecked(true);
                    }
                    if (p.getSexo().equals(Paciente.MASCULINO)) {
                        rbMasculino.setChecked(true);
                    }

                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void atualizarPaciente() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(this);
        alerta.setTitle("Dados Paciente: ");
        alerta.setIcon(android.R.drawable.ic_menu_edit);

        alerta.setNeutralButton("Cancelar", null);
        alerta.setPositiveButton("Atualizar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Paciente paciente = new Paciente();
                paciente.setNome(etNome.getText().toString());
                paciente.setTelefone(etTelefone.getText().toString());
                paciente.setCep(etCep.getText().toString());
                paciente.setComplemento(etComplemento.getText().toString());
                paciente.setCpf(etCpf.getText().toString());
                paciente.setDataNascimento(etDataNascimento.getText().toString());
                paciente.setEmail(etEmail.getText().toString());
                paciente.setLogradouro(etLogradouro.getText().toString());
                paciente.setNumero(etNumero.getText().toString());
                paciente.setObservacao(etObservacao.getText().toString());
                paciente.setCidade((Cidade) spCidade.getSelectedItem());
                paciente.setConvenio((Convenio) spConvenio.getSelectedItem());

                String erro = "";
                if (paciente == null) {
                    erro = "Nome";
                } else {
                    if (spCidade.getSelectedItemPosition() == 0) {
                        erro = "Cidade";
                    } else {
                        if (spConvenio.getSelectedItemPosition() == 0) {
                            erro = "Convenio";
                        }
                    }
                }

                String sexo = "";
                if (rbFeminino.isChecked())
                    sexo = Paciente.FEMININO;
                if (rbMasculino.isChecked())
                    sexo = Paciente.MASCULINO;
                paciente.setSexo(sexo);

                if (!erro.isEmpty()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CadPacientesActivity.this);
                    alert.setTitle("Atenção!!!");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("Você deve selecionar o campo " + erro);
                    alert.setNeutralButton("OK", null);
                    alert.show();
                } else {
                    ref.child("pacientes").child(idPaciente).setValue(paciente);
                    Toast.makeText(CadPacientesActivity.this, "Dados atualizados com sucesso", Toast.LENGTH_LONG).show();
                    limpar();
                }
            }
        });
        alerta.show();
    }

    public void limpar() {
        etNome.setText("");
        etDataNascimento.setText("");
        etTelefone.setText("");
        etCpf.setText("");
        etEmail.setText("");
        etLogradouro.setText("");
        etComplemento.setText("");
        etNumero.setText("");
        etCep.setText("");
        etObservacao.setText("");
        rbMasculino.setChecked(false);
        rbFeminino.setChecked(false);
        spConvenio.setSelection(0);
        spCidade.setSelection(0);
    }

}
